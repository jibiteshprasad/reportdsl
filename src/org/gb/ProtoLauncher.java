package org.gb;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.gb.decorator.ExcelGenerator;
import org.gb.decorator.GenerateData;
import org.gb.decorator.GenerateDecorator;
import org.gb.pojo.data.SimpleTablePojo;
import org.gb.pojo.decorator.TableDecoratorPojo;

public class ProtoLauncher {
	
	public static void main(String args[]) {
		GenerateDecorator decogenerator = new GenerateDecorator();
		TableDecoratorPojo decorator = decogenerator.compile();
		
		GenerateData datagenerator = new GenerateData();
		SimpleTablePojo table = datagenerator.gTHC();
		
		ExcelGenerator excelgenerator = new ExcelGenerator();
		excelgenerator.init();
		excelgenerator.generateExcel(table, decorator);
		
		SXSSFWorkbook wb = excelgenerator.getWorkBook();
		
		try {
			FileOutputStream out = new FileOutputStream("/home/neo/Documents/Bofa/ReportBofA/ouput/report.xlsx");
			wb.write(out);
			out.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
