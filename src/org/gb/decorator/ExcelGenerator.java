package org.gb.decorator;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.gb.pojo.data.SimpleHeaderPojo;
import org.gb.pojo.data.SimpleRowPojo;
import org.gb.pojo.data.SimpleTablePojo;
import org.gb.pojo.decorator.ColumnDecoratorPojo;
import org.gb.pojo.decorator.FilterDecoratorPojo;
import org.gb.pojo.decorator.HeaderDecoratorPojo;
import org.gb.pojo.decorator.TableDecoratorPojo;
import org.gb.pojo.decorator.inter.IFilterDecorator;
import org.gb.pojo.excel.constants.DecoratorAlign;
import org.gb.pojo.excel.constants.DecoratorColor;
import org.gb.pojo.excel.constants.DecoratorFont;

public class ExcelGenerator {
	
	private SXSSFWorkbook workbook;
	private Sheet sheet;
	int rownum = 0;
	
	public void init(){
		workbook = new SXSSFWorkbook();
	}
	
	public void generateSheet(String name){
		sheet = workbook.createSheet(name);
	}
	
	public void generateExcel( SimpleTablePojo table, TableDecoratorPojo decorator){
		generateSheet(decorator.getSheetname());
		generateHeader(table.getHeader(), decorator.getHeaderlist());
		generateRows( table.getHeader(), table.getRowlist(), decorator.getColumnlist());
	}
	
	public void generateRows( SimpleHeaderPojo shp, List<SimpleRowPojo> srpl, List<ColumnDecoratorPojo> cdl){
		for( SimpleRowPojo srp : srpl){
			Row row = sheet.createRow( rownum);
			int cellnum = 0;
			rownum++;
			List<String> rowentry = srp.getColumnvalue();
			for( String h : shp.getHeaders()){
				ColumnDecoratorPojo cdp = getColumnDecorator(h, cdl);
				Cell cell = row.createCell(cellnum);
				CellStyle cellstyle = workbook.createCellStyle();
				Font cellfont = workbook.createFont();
				cellfont.setFontName(getFontType(cdp.getFont()));
				cellfont.setColor(getColorType(cdp.getColor()));
				cellstyle.setAlignment(getAlignment(cdp.getAlign()));
				
				cellstyle.setFont(cellfont);
				cell.setCellStyle(cellstyle);
				cell.setCellValue(rowentry.get(cellnum));
				
//				Implement the filter here
				List<IFilterDecorator> fdp = cdp.getFilters();
				for( IFilterDecorator filter : fdp ){
					filter.filter(cell, this);
				}
				cellnum++;
			}
		}
	}
	
	public void generateHeader( SimpleHeaderPojo shp, List<HeaderDecoratorPojo> hdl){
		int cellnum = 0;
		Row row = sheet.createRow(rownum);
		for( String header : shp.getHeaders() ){
			HeaderDecoratorPojo hdp = getHeaderDecorator(header, hdl);
			Cell cell = row.createCell(cellnum);
			CellStyle cellstyle = workbook.createCellStyle();
			Font cellfont = workbook.createFont();
			cellfont.setFontName(getFontType(hdp.getFont()));
			cellfont.setColor(getColorType(hdp.getColor()));
			cellstyle.setAlignment(getAlignment(hdp.getAlign()));
			cellstyle.setFont(cellfont);
			cell.setCellStyle(cellstyle);
			cell.setCellValue(header);
			cellnum++;
		}
		rownum++;
	}
	
	public short getColorType( DecoratorColor dc){
		if( dc.getColor().equalsIgnoreCase("blue"))
			return ((short) 0xc);
		if( dc.getColor().equalsIgnoreCase("red"))
			return (Font.COLOR_RED);
		if( dc.getColor().equalsIgnoreCase("black"))
			return (Font.COLOR_NORMAL);
		return -888;
	}
	
	public String getFontType(DecoratorFont df){
		if( df.getFont().equalsIgnoreCase("arial"))
			return HSSFFont.FONT_ARIAL;
		if( df.getFont().equalsIgnoreCase("Courier"))
			return "Courier";
		if( df.getFont().equalsIgnoreCase("Tahoma"))
			return "Tahoma";
		return null;
	}
	
	public short getAlignment(DecoratorAlign d){
		if( d.getAlignment().equalsIgnoreCase("left"))
			return CellStyle.ALIGN_LEFT;
		if( d.getAlignment().equalsIgnoreCase("right"))
			return CellStyle.ALIGN_RIGHT;
		if( d.getAlignment().equalsIgnoreCase("center"))
			return CellStyle.ALIGN_CENTER;
		return -999;
	}
	
	public ColumnDecoratorPojo getColumnDecorator( String name, List<ColumnDecoratorPojo> cdl){
		for( ColumnDecoratorPojo cdp : cdl){
			if( cdp.getName().equals(name))
				return cdp;
		}
		ColumnDecoratorPojo cdp = new ColumnDecoratorPojo();
		cdp.setDefaults();
		cdp.setName(name);
		return cdp;
	}
	
	public HeaderDecoratorPojo getHeaderDecorator( String name, List<HeaderDecoratorPojo> hdl){
		for( HeaderDecoratorPojo hdp : hdl){
			if( hdp.getName().equals(name))
				return hdp;
		}
		HeaderDecoratorPojo hdp = new HeaderDecoratorPojo();
		hdp.setDefaults();
		hdp.setName(name);
		return hdp;
	}
	
	public SXSSFWorkbook getWorkBook(){
		return workbook;
	}
}
