package org.gb.decorator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.gb.grammar.BOFALexer;
import org.gb.grammar.BOFAParser;
import org.gb.pojo.decorator.TableDecoratorPojo;
import org.gb.pojo.decorator.handler.DecoratorColumnHandler;
import org.gb.pojo.decorator.handler.DecoratorFilterHandler;
import org.gb.pojo.decorator.handler.DecoratorHeaderHandler;
import org.gb.pojo.decorator.handler.DecoratorSheetHandler;
import org.gb.pojo.decorator.handler.inter.IDecoratorHandler;

public class GenerateDecorator {
	
	TableDecoratorPojo decorator = new TableDecoratorPojo();
	IDecoratorHandler decorate;

	public TableDecoratorPojo compile(){
		String inputfile = "/home/neo/Documents/Bofa/ReportBofA/input/decorate";
		try {
			InputStream is = new FileInputStream( inputfile);
			ANTLRInputStream input = new ANTLRInputStream(is);
			BOFALexer lexer = new BOFALexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			BOFAParser parser = new BOFAParser(tokens);
			ParseTree tree = parser.bofa();

			//			System.out.println(tree.getChild(0).getText());

			int nodecount1 = tree.getChildCount();
			for( int i = 0; i < nodecount1; i ++){
				ParseTree node1 = tree.getChild(i);
				if( node1.getText().trim().length() == 0)
					continue;
				//				System.out.println("First level: " + node1.getText());

				int nodecount2 = node1.getChildCount();
//				System.out.println("nodecount2 : " + nodecount2);
				for( int j = 0; j < nodecount2; j++){
					ParseTree node2 = node1.getChild(j);
					String text2 = node2.getText();
					if( text2.trim().length() == 0
							|| text2.equals("{")
							|| text2.equals("}"))
						continue;
					if( text2.equalsIgnoreCase("Sheet")){
						decorate = new DecoratorSheetHandler();
						continue;
					}
					if( text2.equalsIgnoreCase("Header")){
						decorate = new DecoratorHeaderHandler();
						continue;
					}
					if( text2.equalsIgnoreCase("Filter")){
						decorate = new DecoratorFilterHandler();
						continue;
					}
					if( text2.equalsIgnoreCase("Column")){
						decorate = new DecoratorColumnHandler();
						continue;
					}
					if( text2.equals("}")){
						decorate = null;
					}
					if (node2.getChildCount() > 1){
						decorate.handle(decorator, node2);
					}
				}
			}
			
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
			e.printStackTrace();
		}
		return decorator;
	}
	
}
