package org.gb.decorator;

import java.util.ArrayList;
import java.util.List;

import org.gb.pojo.data.SimpleHeaderPojo;
import org.gb.pojo.data.SimpleRowPojo;
import org.gb.pojo.data.SimpleTablePojo;

public class GenerateData {
	
	public SimpleTablePojo gTHC(){
		SimpleTablePojo table = new SimpleTablePojo();
		
		SimpleHeaderPojo header = new SimpleHeaderPojo();
		List<String> shl = new ArrayList<>();
		shl.add("SrNo");
		shl.add("Heroes");
		shl.add("BaseStr");
		shl.add("BaseAgi");
		shl.add("BaseInt");
		shl.add("BaseStat");
		shl.add("Damage");
		shl.add("Armor");
		header.setHeaders(shl);
		table.setHeader(header);
		
		List<SimpleRowPojo> rowlist = new ArrayList<>();
		
		SimpleRowPojo row1 = new SimpleRowPojo();
		List<String> scl = new ArrayList<>();
		scl.add("1");
		scl.add("Admiral");
		scl.add("21");
		scl.add("14");
		scl.add("16");
		scl.add("51");
		scl.add("47-57");
		scl.add("3.6");
		row1.setColumnvalue(scl);
		rowlist.add(row1);
		
		SimpleRowPojo row2 = new SimpleRowPojo();
		List<String> scl2 = new ArrayList<>();
		scl2.add("2");
		scl2.add("Alchemist");
		scl2.add("25");
		scl2.add("11");
		scl2.add("25");
		scl2.add("61");
		scl2.add("49-58");
		scl2.add("0.6");
		row2.setColumnvalue(scl2);
		rowlist.add(row2);
		
		SimpleRowPojo row3 = new SimpleRowPojo();
		List<String> scl3 = new ArrayList<>();
		scl3.add("3");
		scl3.add("Anti-Mage");
		scl3.add("20");
		scl3.add("22");
		scl3.add("15");
		scl3.add("57");
		scl3.add("49-53");
		scl3.add("3.1");
		row3.setColumnvalue(scl3);
		rowlist.add(row3);
		
		SimpleRowPojo row4 = new SimpleRowPojo();
		List<String> scl4 = new ArrayList<>();
		scl4.add("4");
		scl4.add("Axe");
		scl4.add("25");
		scl4.add("20");
		scl4.add("14");
		scl4.add("59");
		scl4.add("49-53");
		scl4.add("1.8");
		row4.setColumnvalue(scl4);
		rowlist.add(row4);
		
		table.setRowlist(rowlist);
		return table;
	}
	
	public SimpleTablePojo gTC(){
		return null;
	}
	
	public SimpleTablePojo gTHHC(){
		return null;
	}

}
