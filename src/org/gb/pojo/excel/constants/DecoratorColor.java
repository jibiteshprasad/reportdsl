package org.gb.pojo.excel.constants;

public enum DecoratorColor {
	RED("red"), BLUE("blue"), BLACK("black");
	
	private String color;
	
	private DecoratorColor( String color){
		this.color = color;
	}
	
	public String getColor(){
		return this.color;
	}
}
