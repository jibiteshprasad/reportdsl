package org.gb.pojo.excel.constants;

public enum DecoratorAlign {
	LEFT("left"), CENTER("center"), RIGHT("right");
	
	private String align;
	
	private DecoratorAlign( String align){
		this.align = align;
	}
	
	public String getAlignment(){
		return this.align;
	}
}
