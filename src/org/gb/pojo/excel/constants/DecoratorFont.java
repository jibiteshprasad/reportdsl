package org.gb.pojo.excel.constants;

public enum DecoratorFont {
	TAHOMA("tahoma"), ARIAL("arial"), COURIER("courier");
	
	private String font;
	
	private DecoratorFont( String font){
		this.font = font;
	}
	
	public String getFont(){
		return font;
	}
}
