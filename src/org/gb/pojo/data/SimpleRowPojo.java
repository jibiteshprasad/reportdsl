package org.gb.pojo.data;

import java.util.List;

public class SimpleRowPojo {
	
	private List<String> columnvalue;

	public List<String> getColumnvalue() {
		return columnvalue;
	}

	public void setColumnvalue(List<String> columnvalue) {
		this.columnvalue = columnvalue;
	}
	
	
}
