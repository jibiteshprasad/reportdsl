package org.gb.pojo.data;

import java.util.List;

public class SimpleTablePojo {
	
	private SimpleHeaderPojo header;
	private List<SimpleRowPojo> rowlist;
	
	public SimpleHeaderPojo getHeader() {
		return header;
	}
	public void setHeader(SimpleHeaderPojo header) {
		this.header = header;
	}
	public List<SimpleRowPojo> getRowlist() {
		return rowlist;
	}
	public void setRowlist(List<SimpleRowPojo> rowlist) {
		this.rowlist = rowlist;
	}

	
}
