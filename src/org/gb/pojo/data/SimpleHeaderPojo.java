package org.gb.pojo.data;

import java.util.List;

public class SimpleHeaderPojo {

	private List<String> headers;
	private SimpleHeaderPojo pojo;
	
	public List<String> getHeaders() {
		return headers;
	}
	public void setHeaders(List<String> headers) {
		this.headers = headers;
	}
	public SimpleHeaderPojo getPojo() {
		return pojo;
	}
	public void setPojo(SimpleHeaderPojo pojo) {
		this.pojo = pojo;
	}
	
	
	
}
