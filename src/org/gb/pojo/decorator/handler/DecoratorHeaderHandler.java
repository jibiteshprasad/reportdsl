package org.gb.pojo.decorator.handler;

import org.antlr.v4.runtime.tree.ParseTree;
import org.gb.pojo.decorator.HeaderDecoratorPojo;
import org.gb.pojo.decorator.TableDecoratorPojo;
import org.gb.pojo.decorator.handler.inter.IDecoratorHandler;
import org.gb.pojo.excel.constants.DecoratorAlign;
import org.gb.pojo.excel.constants.DecoratorColor;
import org.gb.pojo.excel.constants.DecoratorFont;

public class DecoratorHeaderHandler implements IDecoratorHandler {

	@Override
	public void handle(TableDecoratorPojo decorator, ParseTree node2) {
		
		HeaderDecoratorPojo header = new HeaderDecoratorPojo();
		header.setDefaults();
		int nodecount3 = node2.getChildCount();
		for( int i = 0; i < nodecount3; i ++ ){
			ParseTree node3 = node2.getChild(i);
			String text3 = node3.getText();
			if ( text3.trim().length() == 0 )
				continue;
			if( text3.equalsIgnoreCase("Name") ){
				String name = node2.getChild(i+2).getText();
				header.setName(name);
			}
			if( text3.equalsIgnoreCase("Color") ){
				String color = node2.getChild(i+2).getText();
				DecoratorColor dc = DecoratorColor.valueOf(color);
				header.setColor(dc);
			}
			if( text3.equalsIgnoreCase("Align") ){
				String align = node2.getChild(i+2).getText();
				DecoratorAlign da = DecoratorAlign.valueOf(align);
				header.setAlign(da);
			}
			if( text3.equalsIgnoreCase("Font") ){
				String font = node2.getChild(i+2).getText();
				DecoratorFont df = DecoratorFont.valueOf(font);
				header.setFont(df);
			}
			
		}
		decorator.addHeader(header);

	}

}
