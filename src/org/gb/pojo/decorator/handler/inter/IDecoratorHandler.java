package org.gb.pojo.decorator.handler.inter;

import org.antlr.v4.runtime.tree.ParseTree;
import org.gb.pojo.decorator.TableDecoratorPojo;

public interface IDecoratorHandler {
	
	public void handle(TableDecoratorPojo decorator, ParseTree node2);

}
