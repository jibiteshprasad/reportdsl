package org.gb.pojo.decorator.handler;

import org.antlr.v4.runtime.tree.ParseTree;
import org.gb.pojo.decorator.FilterDecoratorPojo;
import org.gb.pojo.decorator.TableDecoratorPojo;
import org.gb.pojo.decorator.handler.inter.IDecoratorHandler;

public class DecoratorFilterHandler implements IDecoratorHandler{

	@Override
	public void handle(TableDecoratorPojo decorator, ParseTree node2) {
		FilterDecoratorPojo filter = new FilterDecoratorPojo();
		int nodecount3 = node2.getChildCount();
		for( int i = 0; i < nodecount3; i ++ ){
			ParseTree node3 = node2.getChild(i);
			String text3 = node3.getText();
			if ( text3.trim().length() == 0 )
				continue;
			if( text3.equalsIgnoreCase("Name") ){
				String name = node2.getChild(i+2).getText();
				filter.setName(name);
				System.out.println("<---------->");
				System.out.println("Name: " + name);
				System.out.println("<---------->");
				i = i + 2;
			}
			if( text3.equalsIgnoreCase("Condition") ){
				String condition = node2.getChild(i+2).getText()
						+ node2.getChild(i+3).getText()
						+ node2.getChild(i+4).getText()
						+ node2.getChild(i+5).getText()
						+ node2.getChild(i+6).getText();
				System.out.println("<---------->");
				System.out.println("Condition: " + condition);
				System.out.println("<---------->");
				filter.setCondition(condition);
				i = i + 6;
			}
			if( text3.equalsIgnoreCase("Effect") ){
				String effect = node2.getChild(i+2).getText()
						+ node2.getChild(i+3).getText()
						+ node2.getChild(i+4).getText();
				System.out.println("<---------->");
				System.out.println("Effect: " + effect);
				System.out.println("<---------->");
				filter.setEffect(effect);
				i = i + 4;
			}
			
		}
		decorator.addFilter(filter);
		
	}

}
