package org.gb.pojo.decorator.handler;

import org.antlr.v4.runtime.tree.ParseTree;
import org.gb.pojo.decorator.TableDecoratorPojo;
import org.gb.pojo.decorator.handler.inter.IDecoratorHandler;

public class DecoratorSheetHandler implements IDecoratorHandler {

	@Override
	public void handle(TableDecoratorPojo decorator, ParseTree node2) {
		
		int nodecount3 = node2.getChildCount();
		for( int i = 0; i < nodecount3; i ++ ){
			ParseTree node3 = node2.getChild(i);
			String text3 = node3.getText();
			if ( text3.trim().length() == 0 )
				continue;
			if( text3.equalsIgnoreCase("Name") ){
				decorator.setSheetname(node2.getChild(i+2).getText());
			}			
		}
	}

}
