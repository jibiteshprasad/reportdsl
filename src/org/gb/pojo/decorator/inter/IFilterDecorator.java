package org.gb.pojo.decorator.inter;

import org.apache.poi.ss.usermodel.Cell;
import org.gb.decorator.ExcelGenerator;

public interface IFilterDecorator {
	public boolean filter(Cell cell, ExcelGenerator eg);
}
