package org.gb.pojo.decorator;

import java.util.ArrayList;
import java.util.List;

import org.gb.pojo.decorator.inter.IFilterDecorator;
import org.gb.pojo.excel.constants.DecoratorAlign;
import org.gb.pojo.excel.constants.DecoratorColor;
import org.gb.pojo.excel.constants.DecoratorFont;

public class ColumnDecoratorPojo {
	
	private String name;
	private DecoratorFont font;
	private DecoratorAlign align;
	private DecoratorColor color;
	private List<IFilterDecorator> filters =new ArrayList<>();
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public DecoratorFont getFont() {
		return font;
	}
	public void setFont(DecoratorFont font) {
		this.font = font;
	}
	public DecoratorAlign getAlign() {
		return align;
	}
	public void setAlign(DecoratorAlign align) {
		this.align = align;
	}
	public DecoratorColor getColor() {
		return color;
	}
	public void setColor(DecoratorColor color) {
		this.color = color;
	}
	public List<IFilterDecorator> getFilters() {
		return filters;
	}
	public void addFilter(IFilterDecorator filter) {
		this.filters.add(filter);
	}
	public void setDefaults(){
		color = DecoratorColor.BLACK;
		font = DecoratorFont.ARIAL;
		align = DecoratorAlign.LEFT;
	}
}
