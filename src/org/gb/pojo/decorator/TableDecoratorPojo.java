package org.gb.pojo.decorator;

import java.util.ArrayList;
import java.util.List;

public class TableDecoratorPojo {
	
	private String sheetname;
	private List<HeaderDecoratorPojo> headerlist = new ArrayList<>();
	private List<ColumnDecoratorPojo> columnlist = new ArrayList<>();
	private List<FilterDecoratorPojo> filterlist = new ArrayList<>();
	
	public List<HeaderDecoratorPojo> getHeaderlist() {
		return headerlist;
	}
	public void addHeader(HeaderDecoratorPojo header) {
		this.headerlist.add(header);
	}
	public List<ColumnDecoratorPojo> getColumnlist() {
		return columnlist;
	}
	public void addColumn(ColumnDecoratorPojo column) {
		this.columnlist.add(column);
	}
	public String getSheetname() {
		return sheetname;
	}
	public void setSheetname(String sheetname) {
		this.sheetname = sheetname;
	}
	public List<FilterDecoratorPojo> getFilterlist() {
		return filterlist;
	}
	public void addFilter(FilterDecoratorPojo filter) {
		this.filterlist.add(filter);
	}
	
	public FilterDecoratorPojo getFilter( String name){
		for( FilterDecoratorPojo f : filterlist){
			if ( f.getName().equals(name))
				return f;
		}
		return null;
	}
	
}
