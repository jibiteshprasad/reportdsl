package org.gb.pojo.decorator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Workbook;
import org.gb.decorator.ExcelGenerator;
import org.gb.pojo.decorator.inter.IFilterDecorator;
import org.gb.pojo.excel.constants.DecoratorAlign;
import org.gb.pojo.excel.constants.DecoratorColor;
import org.gb.pojo.excel.constants.DecoratorFont;

public class FilterDecoratorPojo implements IFilterDecorator{
	
	private String name;
	private String condition;
	private String effect;

	@Override
	public boolean filter(Cell cell, ExcelGenerator eg) {
		String[] condarray = condition.split(" ");
		String lhs = cell.getStringCellValue();
		String operand = condarray[1];
		String rhs = condarray[2];
		
		String[] effecta = effect.split(" ");
		String evar = effecta[0];
		String eval = effecta[1]; 
		
		CellStyle cs = cell.getCellStyle();
		Workbook wb = cell.getRow().getSheet().getWorkbook();
		
		if( check(lhs, operand, rhs)){
			if( evar.equalsIgnoreCase("Font")){
				DecoratorFont font = DecoratorFont.valueOf(eval);
				String f = eg.getFontType(font);
				Font cf = wb.createFont();
				cf.setFontName(f);
				cs.setFont(cf);
			}
			if( evar.equalsIgnoreCase("Color")){
				DecoratorColor c = DecoratorColor.valueOf(eval);
				short color = eg.getColorType(c);
				Font cf = wb.createFont();
				cf.setColor(color);
				cs.setFont(cf);
			}
			if( evar.equalsIgnoreCase("Align")){
				DecoratorAlign a = DecoratorAlign.valueOf(eval);
				short align = eg.getAlignment(a);
				cs.setAlignment(align);
			}
			if( evar.equalsIgnoreCase("value")){
				cell.setCellValue(eval);
			}
		}
		return false;
	}
	
	private boolean check(String lhs, String operand, String rhs){
		if( operand.equals("gt")){
			Integer ilhs = Integer.valueOf(lhs);
			Integer irhs = Integer.valueOf(rhs);
			if( ilhs > irhs)
				return true;
			else
				return false;
		}
		if( operand.equals("lt")){
			Integer ilhs = Integer.valueOf(lhs);
			Integer irhs = Integer.valueOf(rhs);
			if( ilhs < irhs)
				return true;
			else
				return false;
		}
		if( operand.equals("eq")){
			if( lhs.equals(rhs))
				return true;
			else
				return false;
		}
		return false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

}
