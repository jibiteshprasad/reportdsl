package org.gb.pojo.decorator;

import java.util.ArrayList;
import java.util.List;

import org.gb.pojo.excel.constants.DecoratorAlign;
import org.gb.pojo.excel.constants.DecoratorColor;
import org.gb.pojo.excel.constants.DecoratorFont;

public class HeaderDecoratorPojo {
	
	private String name;
	private DecoratorColor color;
	private DecoratorFont font;
	private DecoratorAlign align;
	private List<HeaderDecoratorPojo> subheaderlist = new ArrayList<>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public DecoratorColor getColor() {
		return color;
	}
	public void setColor(DecoratorColor color) {
		this.color = color;
	}
	public DecoratorFont getFont() {
		return font;
	}
	public void setFont(DecoratorFont font) {
		this.font = font;
	}
	public DecoratorAlign getAlign() {
		return align;
	}
	public void setAlign(DecoratorAlign align) {
		this.align = align;
	}
	public List<HeaderDecoratorPojo> addHeader() {
		return subheaderlist;
	}
	public void setSubheaderlist(List<HeaderDecoratorPojo> subheaderlist) {
		this.subheaderlist = subheaderlist;
	}
	 public void setDefaults(){
		 color = DecoratorColor.BLUE;
		 font = DecoratorFont.ARIAL;
		 align = DecoratorAlign.CENTER;
	 }
	
}
