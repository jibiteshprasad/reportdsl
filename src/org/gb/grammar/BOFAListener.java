// Generated from /home/neo/Documents/Bofa/ReportBofA/src/org/gb/grammar/BOFA.g4 by ANTLR 4.1
package org.gb.grammar;

import java.util.Set;
import java.util.HashSet;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BOFAParser}.
 */
public interface BOFAListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link BOFAParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(@NotNull BOFAParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BOFAParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(@NotNull BOFAParser.ExprContext ctx);

	/**
	 * Enter a parse tree produced by {@link BOFAParser#func}.
	 * @param ctx the parse tree
	 */
	void enterFunc(@NotNull BOFAParser.FuncContext ctx);
	/**
	 * Exit a parse tree produced by {@link BOFAParser#func}.
	 * @param ctx the parse tree
	 */
	void exitFunc(@NotNull BOFAParser.FuncContext ctx);

	/**
	 * Enter a parse tree produced by {@link BOFAParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(@NotNull BOFAParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link BOFAParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(@NotNull BOFAParser.TypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link BOFAParser#bofa}.
	 * @param ctx the parse tree
	 */
	void enterBofa(@NotNull BOFAParser.BofaContext ctx);
	/**
	 * Exit a parse tree produced by {@link BOFAParser#bofa}.
	 * @param ctx the parse tree
	 */
	void exitBofa(@NotNull BOFAParser.BofaContext ctx);
}