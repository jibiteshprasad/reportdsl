// Generated from /home/neo/Documents/Bofa/ReportBofA/src/org/gb/grammar/BOFA.g4 by ANTLR 4.1
package org.gb.grammar;

import java.util.Set;
import java.util.HashSet;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link BOFAParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface BOFAVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link BOFAParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(@NotNull BOFAParser.ExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link BOFAParser#func}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunc(@NotNull BOFAParser.FuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link BOFAParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(@NotNull BOFAParser.TypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link BOFAParser#bofa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBofa(@NotNull BOFAParser.BofaContext ctx);
}