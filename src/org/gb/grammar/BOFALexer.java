// Generated from /home/neo/Documents/Bofa/ReportBofA/src/org/gb/grammar/BOFA.g4 by ANTLR 4.1
package org.gb.grammar;

import java.util.Set;
import java.util.HashSet;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BOFALexer extends Lexer {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__6=1, T__5=2, T__4=3, T__3=4, T__2=5, T__1=6, T__0=7, ID=8, INT=9, NEWLINE=10, 
		WS=11, SL_COMMENT=12;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"<INVALID>",
		"'Filter'", "'{'", "':'", "'}'", "'Header'", "'Column'", "'Sheet'", "ID", 
		"INT", "NEWLINE", "WS", "SL_COMMENT"
	};
	public static final String[] ruleNames = {
		"T__6", "T__5", "T__4", "T__3", "T__2", "T__1", "T__0", "ID", "INT", "NEWLINE", 
		"WS", "SL_COMMENT"
	};


	public static Set<String> keywords = new HashSet<String>() {{
	    add("Sheet");
	    add("Header");
	    add("Column");
	    add("Filter");
	}};
	boolean notLetterOrDigit(int la) {
	    return !Character.isLetterOrDigit(_input.LA(la));
	}


	public BOFALexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "BOFA.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 10: WS_action((RuleContext)_localctx, actionIndex); break;

		case 11: SL_COMMENT_action((RuleContext)_localctx, actionIndex); break;
		}
	}
	private void WS_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0: _channel = HIDDEN;  break;
		}
	}
	private void SL_COMMENT_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1: _channel = HIDDEN;  break;
		}
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\2\16b\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3"+
		"\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\t\6\t>\n\t\r\t\16\t?\3\n\6\nC\n\n\r\n\16\nD\3\13\6\13H"+
		"\n\13\r\13\16\13I\3\f\6\fM\n\f\r\f\16\fN\3\f\3\f\3\r\3\r\3\r\3\r\7\rW"+
		"\n\r\f\r\16\rZ\13\r\3\r\5\r]\n\r\3\r\3\r\3\r\3\r\3X\16\3\3\1\5\4\1\7\5"+
		"\1\t\6\1\13\7\1\r\b\1\17\t\1\21\n\1\23\13\1\25\f\1\27\r\2\31\16\3\3\2"+
		"\6\4\2C\\c|\3\2\62;\4\2\f\f\"\"\5\2\13\13\17\17\"\"g\2\3\3\2\2\2\2\5\3"+
		"\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2"+
		"\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\3\33\3"+
		"\2\2\2\5\"\3\2\2\2\7$\3\2\2\2\t&\3\2\2\2\13(\3\2\2\2\r/\3\2\2\2\17\66"+
		"\3\2\2\2\21=\3\2\2\2\23B\3\2\2\2\25G\3\2\2\2\27L\3\2\2\2\31R\3\2\2\2\33"+
		"\34\7H\2\2\34\35\7k\2\2\35\36\7n\2\2\36\37\7v\2\2\37 \7g\2\2 !\7t\2\2"+
		"!\4\3\2\2\2\"#\7}\2\2#\6\3\2\2\2$%\7<\2\2%\b\3\2\2\2&\'\7\177\2\2\'\n"+
		"\3\2\2\2()\7J\2\2)*\7g\2\2*+\7c\2\2+,\7f\2\2,-\7g\2\2-.\7t\2\2.\f\3\2"+
		"\2\2/\60\7E\2\2\60\61\7q\2\2\61\62\7n\2\2\62\63\7w\2\2\63\64\7o\2\2\64"+
		"\65\7p\2\2\65\16\3\2\2\2\66\67\7U\2\2\678\7j\2\289\7g\2\29:\7g\2\2:;\7"+
		"v\2\2;\20\3\2\2\2<>\t\2\2\2=<\3\2\2\2>?\3\2\2\2?=\3\2\2\2?@\3\2\2\2@\22"+
		"\3\2\2\2AC\t\3\2\2BA\3\2\2\2CD\3\2\2\2DB\3\2\2\2DE\3\2\2\2E\24\3\2\2\2"+
		"FH\t\4\2\2GF\3\2\2\2HI\3\2\2\2IG\3\2\2\2IJ\3\2\2\2J\26\3\2\2\2KM\t\5\2"+
		"\2LK\3\2\2\2MN\3\2\2\2NL\3\2\2\2NO\3\2\2\2OP\3\2\2\2PQ\b\f\2\2Q\30\3\2"+
		"\2\2RS\7\61\2\2ST\7\61\2\2TX\3\2\2\2UW\13\2\2\2VU\3\2\2\2WZ\3\2\2\2XY"+
		"\3\2\2\2XV\3\2\2\2Y\\\3\2\2\2ZX\3\2\2\2[]\7\17\2\2\\[\3\2\2\2\\]\3\2\2"+
		"\2]^\3\2\2\2^_\7\f\2\2_`\3\2\2\2`a\b\r\3\2a\32\3\2\2\2\t\2?DINX\\";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}