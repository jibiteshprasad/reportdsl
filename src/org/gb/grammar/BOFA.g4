/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

grammar BOFA;

// parser rules
@header {
import java.util.Set;
import java.util.HashSet;
}

@members {
public static Set<String> keywords = new HashSet<String>() {{
    add("Sheet");
    add("Header");
    add("Column");
    add("Filter");
}};
boolean notLetterOrDigit(int la) {
    return !Character.isLetterOrDigit(_input.LA(la));
}
}
bofa:	(func )+ ;

func:	type '{' expr '}' ;

type:	'Header'
    |	'Column'
    |   'Filter'
    |   'Sheet'
	;

expr:
        expr ':'<assoc=right> expr NEWLINE
    |   ID
    |   INT
    ;

ID	:	[a-zA-Z]+ ;

INT :   [0-9]+ ;

NEWLINE : [ \n]+;

WS  :   [ \t\r]+ -> channel(HIDDEN) ;

SL_COMMENT
    :   '//' .*? '\r'? '\n' -> channel(HIDDEN)
    ;
